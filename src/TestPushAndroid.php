<?php
/**
 * TestPushAndroid.php - fire-ice-push
 * @author Johannes Heimbach <info@mediengstalt.de>
 * @created 19.10.17 - 15:29
 */

namespace FireIcePush;

use GuzzleHttp\Client;

class TestPushAndroid
{
    private const SERVER_KEY = "AAAAhndeHAg:APA91bEp6ZF_4Y6SYv_DmbvohwI_9m8YqpHNBREG3o3YlnE36DPZUZ0sPIxvmcrMHdg7hA1XcUtkheSOG7P0yXSg0ANl6ByijQe5rKJ7AAVpNSJS1mMiBURBOAFvGUvwBlOZAB-786ri";
    private $deviceToken;
    private $httpConn;

    /**
     * TestPushAndroid constructor.
     * @param string $deviceToken
     */
    public function __construct(string $deviceToken)
    {
        $this->deviceToken = $deviceToken;
        $this->httpConn = new Client([
            'base_uri' => 'https://fcm.googleapis.com/fcm/send',
            'headers' => [
                'Authorization' => 'key=' . TestPushAndroid::SERVER_KEY,
                'Content-Type' => 'application/json'
            ]
        ]);
    }

    public function sendNotification($title, $message)
    {
        $notification = $this->createNotificationData($title, $message);
        $response = $this->httpConn->post('', [
            'body' => json_encode($notification)
        ]);

        var_dump(json_decode($response->getBody()->getContents()));
    }

    /**
     * @param $title
     * @param $message
     * @return array
     */
    protected function createNotificationData($title, $message): array
    {
        $notifcation = [];
        $notifcation['registration_ids'] = [$this->deviceToken];
        $notifcation['priority'] = 'high';
        $notifcation['content_available'] = true;

        $data = [];
        $data['title'] = $title;
        $data['body'] = $message;
        $data['vibrationPattern'] = [200, 300];
        $data['soundname'] = 'default';
        $data['style'] = 'inbox';
        $data['summaryText'] = 'There are %n% notifications';
        $data['image'] = 'https://www.swipeandmeet.com/images/thumbs/14cf99b445c73a20c1e38e0e08b9325e5910600.jpg';
        $data['image-type'] = 'circle';

        $news = [];
        $news['type'] = 'like';
        $news['link_to'] = [
            'screen' => 'profile',
            'profile_id' => 5941
        ];
        $data['news'] = $news;

        $notifcation['data'] = $data;

        return $notifcation;
    }

}
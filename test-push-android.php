<?php
/**
 * test-push-android.php - fire-ice-push
 * @author Johannes Heimbach <info@mediengstalt.de>
 * @created 19.10.17 - 15:27
 */

use FireIcePush\TestPushAndroid;

require __DIR__ . '/vendor/autoload.php';

$testAndroid = new TestPushAndroid("f4wx4NTs9hw:APA91bFRaK6AjPdVkYt_grvS1bIVBJXCw7idZV9TfNSRsqD7dYV_ej2hHdYboNsHaSGVswKJFvSt59JcfgGdf6hOUn2czeyF4e3DmUFNr6xmUMbnSmhweg4tyTadknN1CTnB-DeCeo-p");
$testAndroid->sendNotification('Test', 'this is a test');